/*
 * digitful - A 2D lighting simulation in OpenGL
 * Copyright (C) 2020-2022 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <digitful/renderer.h>
#include <stdio.h>
#include <stdlib.h>
#include <digitful/gl_utils.h>

static bool is_inited = false;

static void glfw_error_callback(int code, const char *desc) {
  printf("Received glfw error [%i]: %s\n", code, desc);
}

void ren_init() {
  if(is_inited) return;
  is_inited = true;

  if(!glfwInit()) {
    puts("Failed to initialize glfw");
    exit(EXIT_FAILURE);
  }
  glfwSetErrorCallback(glfw_error_callback);
}

void ren_deinit() {
  if(!is_inited) return;
  is_inited = false;

  glfwTerminate();
}



static void renderer_close_callback(GLFWwindow *window) {
  Renderer *self = glfwGetWindowUserPointer(window);

  self->should_close = true;
}

static void renderer_resize_callback(GLFWwindow *window, int width, int height) {
  Renderer *self = glfwGetWindowUserPointer(window);

  self->was_resized = true;
  self->width = width;
  self->height = height;

  self->should_refresh_offscreen_fbo = self->has_offscreen_fbo;
}

static void renderer_key_callback(GLFWwindow *window, int key, int scancode, int action, int mods) {
  (void) scancode, (void) mods; // This is a trick to silence compiler warnings about unused parameters.
  Renderer *self = glfwGetWindowUserPointer(window);

  if(key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    self->was_esc_pressed = true;
  } else if(key == GLFW_KEY_BACKSPACE && action == GLFW_RELEASE) {
    self->was_backspace_pressed = true;
  }
}

static void renderer_mouse_callback(GLFWwindow *window, int button, int action, int mods) {
  (void) mods;
  Renderer *self = glfwGetWindowUserPointer(window);

  if(button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE) {
    self->was_mouse_pressed = true;
  }
}

static void gl_debug_callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei msg_size, const char *msg, const void *user_param) {
  (void) msg_size, (void) user_param;
  const char *source_str, *type_str, *severity_str;
  switch(source) {
    case GL_DEBUG_SOURCE_API: source_str = "API"; break;
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM: source_str = "WINDOW_SYSTEM"; break;
    case GL_DEBUG_SOURCE_SHADER_COMPILER: source_str = "SHADER_COMPILER"; break;
    case GL_DEBUG_SOURCE_THIRD_PARTY: source_str = "THIRD_PARTY"; break;
    case GL_DEBUG_SOURCE_APPLICATION: source_str = "APPLICATION"; break;
    case GL_DEBUG_SOURCE_OTHER: source_str = "OTHER"; break;
    default: source_str = "?"; break;
  }
  switch(type) {
    case GL_DEBUG_TYPE_ERROR: type_str = "ERROR"; break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: type_str = "DEPRECATED_BEHAVIOR"; break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: type_str = "UNDEFINED_BEHAVIOR"; break;
    case GL_DEBUG_TYPE_PORTABILITY: type_str = "PORTABILITY"; break;
    case GL_DEBUG_TYPE_PERFORMANCE: type_str = "PERFORMANCE"; break;
    case GL_DEBUG_TYPE_MARKER: type_str = "MARKER"; break;
    case GL_DEBUG_TYPE_PUSH_GROUP: type_str = "PUSH_GROUP"; break;
    case GL_DEBUG_TYPE_POP_GROUP: type_str = "POP_GROUP"; break;
    case GL_DEBUG_TYPE_OTHER: type_str = "OTHER"; break;
    default: type_str = "?"; break;
  }
  switch(severity) {
    case GL_DEBUG_SEVERITY_HIGH: severity_str = "HIGH"; break;
    case GL_DEBUG_SEVERITY_MEDIUM: severity_str = "MEDIUM"; break;
    case GL_DEBUG_SEVERITY_LOW: severity_str = "LOW"; break;
    case GL_DEBUG_SEVERITY_NOTIFICATION: severity_str = "NOTIFICATION"; break;
    default: severity_str = "?"; break;
  }
  printf("Received an OpenGL debug message from source %s of type %s, ID %u and severity %s:\n%s\n\n", source_str, type_str, id, severity_str, msg);
}

bool renderer_init(Renderer *self, int width, int height, const char *title) {
  self->width = width;
  self->height = height;

  self->should_close = false;
  self->was_resized = false;
  self->mousex = 0;
  self->mousey = 0;
  self->was_esc_pressed = false;
  self->was_mouse_pressed = false;
  self->was_backspace_pressed = false;

  self->has_offscreen_fbo = false;
  self->should_blit_offscreen_fbo = true;
  self->offscreen_fbo = 0;
  self->offscreen_fbo_tex = 0;
  self->should_refresh_offscreen_fbo = true;

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  //glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);

  self->window = glfwCreateWindow(width, height, title, NULL, NULL);
  if(self->window == NULL) {
    puts("Failed to create a window for renderer");
    return true;
  }

  glfwSetWindowUserPointer(self->window, self);
  glfwSetWindowCloseCallback(self->window, renderer_close_callback);
  glfwSetFramebufferSizeCallback(self->window, renderer_resize_callback);
  glfwSetKeyCallback(self->window, renderer_key_callback);
  glfwSetMouseButtonCallback(self->window, renderer_mouse_callback);

  context_init(&self->ctx);
  renderer_make_ctx_curr(self);
  if(!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
    puts("Failed to load OpenGL");

    glfwDestroyWindow(self->window);
    return true;
  }
  printf("Welcome to OpenGL %s!\n", glGetString(GL_VERSION));
  int context_flags;
  glGetIntegerv(GL_CONTEXT_FLAGS, &context_flags);
  if(context_flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
    glDebugMessageCallback(gl_debug_callback, NULL);
  }

  return false;
}

Renderer* renderer_new(int width, int height, const char *title) {
  Renderer *result = malloc(sizeof(Renderer));
  if(renderer_init(result, width, height, title)) {
    free(result);
    return NULL;
  }
  return result;
}

void renderer_deinit(Renderer *self) {
  glDeleteTextures(1, &self->offscreen_fbo_tex);
  glDeleteFramebuffers(1, &self->offscreen_fbo);
  context_deinit(&self->ctx);

  glfwDestroyWindow(self->window);
}

void renderer_destroy(Renderer *self) {
  renderer_deinit(self);
  free(self);
}

bool renderer_begin(Renderer *self) {
  renderer_make_ctx_curr(self);

  if(self->has_offscreen_fbo) {
    if(self->offscreen_fbo == 0) {
      glGenFramebuffers(1, &self->offscreen_fbo);
    }
    ctx_bind_fbo(self->offscreen_fbo);

    if(self->should_refresh_offscreen_fbo) {
      glDeleteTextures(1, &self->offscreen_fbo_tex);
      self->offscreen_fbo_tex = tex2d_new(self->width, self->height, GL_RGB, GL_NEAREST, GL_NEAREST);

      glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, self->offscreen_fbo_tex, 0);
      GLenum fbo_status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
      if(fbo_status != GL_FRAMEBUFFER_COMPLETE) {
        printf("Failed to refresh off-screen framebuffer, framebuffer status: %i\n", fbo_status);
        return true;
      }

      self->should_refresh_offscreen_fbo = false;
    }
  }

  ctx_viewport(0, 0, self->width, self->height);

  return false;
}

void renderer_end(Renderer *self) {
  if(self->has_offscreen_fbo && self->should_blit_offscreen_fbo) {
    ctx_bind_draw_fbo(0);

    ctx_use_shader(simple_tex_shader);
    glBindTexture(GL_TEXTURE_2D, self->offscreen_fbo_tex);
    glBindVertexArray(tex_overlay_vao);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
  }

  double x, y;
  glfwGetCursorPos(self->window, &x, &y);
  self->mousex = x;
  self->mousey = y;

  glfwSwapBuffers(self->window);
  glfwPollEvents();
}

void renderer_make_ctx_curr(Renderer *self) {
  glfwMakeContextCurrent(self->window);
  curr_ctx = &self->ctx;
}
