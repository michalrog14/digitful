/*
 * digitful - A 2D lighting simulation in OpenGL
 * Copyright (C) 2020-2022 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <digitful/gres.h>
#include <stdio.h>
#include <digitful/utils.h>

static bool is_inited = false;
static HashMap gres_by_name;

static size_t res_hash(void *res) {
  return hash_str(((Resource*) res)->name);
}

static bool res_equals(void *a, void *b) {
  return strcmp(((Resource*) a)->name, ((Resource*) b)->name) == 0;
}

void gres_init() {
  if(is_inited) return;
  is_inited = true;

  hashmap_init(&gres_by_name, sizeof(Resource), res_hash, res_equals);
}

void gres_deinit() {
  if(!is_inited) return;
  is_inited = false;

  for(size_t i = 0; i < gres_by_name.bucketc; i++) {
    if(gres_by_name.buckets[i] != NULL && gres_by_name.buckets[i] != HASHMAP_UNUSED) {
      resource_deinit(gres_by_name.buckets[i]);
    }
  }
  hashmap_deinit(&gres_by_name);
}

Resource* gres_get(const char *name) {
  Resource *res = hashmap_get(&gres_by_name, &(Resource) { .name = (char*) name });
  if(resource_get(res) == NULL) {
    return NULL;
  }
  return res;
}

void gres_release(Resource *res) {
  resource_release(res);
}

void gres_register(Resource res) {
  Resource *old = hashmap_get(&gres_by_name, &res);
  if(old != NULL) {
    resource_deinit(old);
  }
  hashmap_set(&gres_by_name, &res);
}

void gres_register_file(const char *name, const char *filename, bool should_append_null) {
  printf("Global resource '%s' is file '%s'\n", name, filename);
  Resource res;
  resource_init_from_file(&res, name, filename, should_append_null);
  gres_register(res);
}

void gres_register_mem(const char *name, unsigned char *data, size_t size) {
  printf("Global resource '%s' is mem at %p of size %zu bytes\n", name, data, size);
  Resource res;
  resource_init_from_mem(&res, name, data, size);
  gres_register(res);
}
