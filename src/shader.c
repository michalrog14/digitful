/*
 * digitful - A 2D lighting simulation in OpenGL
 * Copyright (C) 2020-2022 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <digitful/shader.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <digitful/gres.h>
#include <digitful/utils.h>

bool shader_stage_init_from_mem(ShaderStage *self, const char *source, GLenum type) {
  self->id = glCreateShader(type);

  glShaderSource(self->id, 1, (const char**) &source, NULL);
  glCompileShader(self->id);

  int did_compile;
  glGetShaderiv(self->id, GL_COMPILE_STATUS, &did_compile);
  if(!did_compile) {
    char info_log[2048];
    glGetShaderInfoLog(self->id, 2048, NULL, info_log);
    printf("Failed to compile shader stage:\n%s\n", info_log);

    glDeleteShader(self->id);
    return true;
  }
  return false;
}

bool shader_stage_init_from_file(ShaderStage *self, const char *filename, GLenum type) {
  char *source = (char*) read_file(filename, true, NULL);
  if(source == NULL) {
    printf("Failed to load shader stage source file '%s': %s\n", filename, strerror(errno));
    return true;
  }
  bool result = shader_stage_init_from_mem(self, source, type);
  free(source);
  return result;
}

bool shader_stage_init_from_gres(ShaderStage *self, const char *name, GLenum type) {
  Resource *res = gres_get(name);
  if(res == NULL) {
    return true;
  }
  bool result = shader_stage_init_from_mem(self, (char*) res->data, type);
  gres_release(res);
  return result;
}

ShaderStage* shader_stage_new_from_mem(const char *source, GLenum type) {
  ShaderStage *result = malloc(sizeof(ShaderStage));
  if(shader_stage_init_from_mem(result, source, type)) {
    free(result);
    return NULL;
  }
  return result;
}

ShaderStage* shader_stage_new_from_file(const char *filename, GLenum type) {
  ShaderStage *result = malloc(sizeof(ShaderStage));
  if(shader_stage_init_from_file(result, filename, type)) {
    free(result);
    return NULL;
  }
  return result;
}

ShaderStage* shader_stage_new_from_gres(const char *name, GLenum type) {
  ShaderStage *result = malloc(sizeof(ShaderStage));
  if(shader_stage_init_from_gres(result, name, type)) {
    free(result);
    return NULL;
  }
  return result;
}

void shader_stage_deinit(ShaderStage *self) {
  glDeleteShader(self->id);
}

void shader_stage_destroy(ShaderStage *self) {
  shader_stage_deinit(self);
  free(self);
}



void shader_init(Shader *self) {
  self->id = glCreateProgram();
}

static bool shader_init_from_stages(Shader *self, ShaderStage *vert_stage, ShaderStage *frag_stage) {
  shader_init(self);

  if(vert_stage == NULL || frag_stage == NULL) {
    if(vert_stage != NULL) {
      shader_stage_destroy(vert_stage);
    }
    if(frag_stage != NULL) {
      shader_stage_destroy(frag_stage);
    }
    return true;
  }

  shader_attach(self, vert_stage);
  shader_attach(self, frag_stage);
  bool result = shader_link(self);

  shader_stage_destroy(vert_stage);
  shader_stage_destroy(frag_stage);

  if(result) {
    shader_deinit(self);
  }

  return result;
}

bool shader_init_from_mem(Shader *self, const char *vert_source, const char *frag_source) {
  ShaderStage *vert_stage = shader_stage_new_from_mem(vert_source, GL_VERTEX_SHADER);
  ShaderStage *frag_stage = shader_stage_new_from_mem(frag_source, GL_FRAGMENT_SHADER);
  return shader_init_from_stages(self, vert_stage, frag_stage);
}

bool shader_init_from_files(Shader *self, const char *vert_filename, const char *frag_filename) {
  ShaderStage *vert_stage = shader_stage_new_from_file(vert_filename, GL_VERTEX_SHADER);
  ShaderStage *frag_stage = shader_stage_new_from_file(frag_filename, GL_FRAGMENT_SHADER);
  return shader_init_from_stages(self, vert_stage, frag_stage);
}

bool shader_init_from_gres(Shader *self, const char *vert_name, const char *frag_name) {
  ShaderStage *vert_stage = shader_stage_new_from_gres(vert_name, GL_VERTEX_SHADER);
  ShaderStage *frag_stage = shader_stage_new_from_gres(frag_name, GL_FRAGMENT_SHADER);
  return shader_init_from_stages(self, vert_stage, frag_stage);
}

Shader* shader_new() {
  Shader *result = malloc(sizeof(Shader));
  shader_init(result);
  return result;
}

Shader* shader_new_from_mem(const char *vert_source, const char *frag_source) {
  Shader *result = malloc(sizeof(Shader));
  if(shader_init_from_mem(result, vert_source, frag_source)) {
    free(result);
    return NULL;
  }
  return result;
}

Shader* shader_new_from_files(const char *vert_filename, const char *frag_filename) {
  Shader *result = malloc(sizeof(Shader));
  if(shader_init_from_files(result, vert_filename, frag_filename)) {
    free(result);
    return NULL;
  }
  return result;
}

Shader* shader_new_from_gres(const char *vert_name, const char *frag_name) {
  Shader *result = malloc(sizeof(Shader));
  if(shader_init_from_gres(result, vert_name, frag_name)) {
    free(result);
    return NULL;
  }
  return result;
}

void shader_deinit(Shader *self) {
  glDeleteProgram(self->id);
}

void shader_destroy(Shader *self) {
  shader_deinit(self);
  free(self);
}

void shader_attach(Shader *self, ShaderStage *stage) {
  glAttachShader(self->id, stage->id);
}

bool shader_link(Shader *self) {
  glLinkProgram(self->id);

  int did_link;
  glGetProgramiv(self->id, GL_LINK_STATUS, &did_link);
  if(!did_link) {
    char info_log[2048];
    glGetProgramInfoLog(self->id, 2048, NULL, info_log);
    printf("Failed to link shader:\n%s\n", info_log);
    return true;
  }

  return false;
}
