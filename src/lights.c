/*
 * digitful - A 2D lighting simulation in OpenGL
 * Copyright (C) 2020-2022 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <digitful/lights.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <digitful/context.h>
#include <digitful/gl_utils.h>
#include <digitful/shader.h>
#include <digitful/utils.h>

static bool is_inited = false;
static Shader *add_shader, *apply_shader, *light_shader, *shadow_shader;

void lights_init() {
  if(is_inited) return;
  is_inited = true;

  puts("Creating add shader...");
  add_shader = shader_new_from_gres("digitful/add.vert", "digitful/add.frag");
  if(add_shader == NULL) {
    exit(EXIT_FAILURE);
  }

  puts("Creating apply shader...");
  apply_shader = shader_new_from_gres("digitful/apply.vert", "digitful/apply.frag");
  if(apply_shader == NULL) {
    exit(EXIT_FAILURE);
  }

  puts("Creating light shader...");
  light_shader = shader_new_from_gres("digitful/light.vert", "digitful/light.frag");
  if(light_shader == NULL) {
    exit(EXIT_FAILURE);
  }

  puts("Creating shadow shader...");
  shadow_shader = shader_new_from_gres("digitful/shadow.vert", "digitful/shadow.frag");
  if(shadow_shader == NULL) {
    exit(EXIT_FAILURE);
  }
}

void lights_deinit() {
  if(!is_inited) return;
  is_inited = false;

  puts("Destroying add, apply, light and shadow shaders...");
  shader_destroy(add_shader);
  shader_destroy(apply_shader);
  shader_destroy(light_shader);
  shader_destroy(shadow_shader);
}



void light_obstacle_init(LightObstacle *self, float r, float g, float b, float xs[], float ys[], size_t vertexc, bool is_closed) {
  self->r = r;
  self->g = g;
  self->b = b;

  size_t vertices_size = sizeof(float) * vertexc;
  self->xs = malloc(vertices_size);
  self->ys = malloc(vertices_size);
  memcpy(self->xs, xs, vertices_size);
  memcpy(self->ys, ys, vertices_size);

  self->vertexc = vertexc;
  self->is_closed = is_closed;
}

LightObstacle* light_obstacle_new(float r, float g, float b, float xs[], float ys[], size_t vertexc, bool is_closed) {
  LightObstacle *result = malloc(sizeof(LightObstacle));
  light_obstacle_init(result, r, g, b, xs, ys, vertexc, is_closed);
  return result;
}

void light_obstacle_deinit(LightObstacle *self) {
  free(self->xs);
  free(self->ys);
}

void light_obstacle_destroy(LightObstacle *self) {
  light_obstacle_deinit(self);
  free(self);
}

LightObstacle* light_obstacle_copy(LightObstacle *self) {
  return light_obstacle_new(self->r, self->g, self->b, self->xs, self->ys, self->vertexc, self->is_closed);
}

void light_obstacle_move(LightObstacle *self, float offset_x, float offset_y) {
  for(size_t i = 0; i < self->vertexc; i++) {
    self->xs[i] += offset_x;
    self->ys[i] += offset_y;
  }
}



bool light_map_init(LightMap *self, int width, int height) {
  self->width = -1;
  self->height = -1;
  self->gamma = 2.2;

  self->fbo = 0;
  self->tex = 0;

  self->shadow_vbo = 0;
  self->shadow_vao = 0;
  self->tex_copy = 0;

  glGenFramebuffers(1, &self->fbo);
  if(light_map_resize(self, width, height, true)) {
    glDeleteFramebuffers(1, &self->fbo);
    self->fbo = 0;
    return true;
  }

  glGenBuffers(1, &self->shadow_vbo);
  glBindBuffer(GL_ARRAY_BUFFER, self->shadow_vbo);

  glGenVertexArrays(1, &self->shadow_vao);
  glBindVertexArray(self->shadow_vao);
  VertexAttribute attrs[] = {{GL_FLOAT, 2}};
  set_vao_attributes(attrs, array_size(attrs));

  return false;
}

LightMap* light_map_new(int width, int height) {
  LightMap *result = malloc(sizeof(LightMap));
  if(light_map_init(result, width, height)) {
    free(result);
    return NULL;
  }
  return result;
}

void light_map_deinit(LightMap *self) {
  glDeleteFramebuffers(1, &self->fbo);
  glDeleteTextures(1, &self->tex);

  glDeleteBuffers(1, &self->shadow_vbo);
  glDeleteVertexArrays(1, &self->shadow_vao);
  glDeleteTextures(1, &self->tex_copy);
}

void light_map_destroy(LightMap *self) {
  light_map_deinit(self);
  free(self);
}

void light_map_center_view(LightMap *self, float x, float y, float w, float h) {
  self->view_x = x - w / 2.0;
  self->view_y = y - h / 2.0;
  self->view_w = w;
  self->view_h = h;
}

bool light_map_resize(LightMap *self, int width, int height, bool should_center_view) {
  if(self->width == width && self->height == height) {
    return false;
  }

  self->width = width;
  self->height = height;
  if(should_center_view) {
    light_map_center_view(self, 0.0, 0.0, width, height);
  }

  ctx_push();

  glDeleteTextures(1, &self->tex);
  self->tex = tex2d_new(self->width, self->height, GL_RGB16F, GL_LINEAR, GL_LINEAR);

  ctx_bind_fbo(self->fbo);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, self->tex, 0);
  GLenum fbo_status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if(fbo_status != GL_FRAMEBUFFER_COMPLETE) {
    printf("Failed to create a framebuffer for light map, framebuffer status: %i\n", fbo_status);

    glDeleteTextures(1, &self->tex);
    self->tex = 0;
    return true;
  }

  glDeleteTextures(1, &self->tex_copy);
  self->tex_copy = tex2d_new(width, height, GL_RGB16F, GL_NEAREST, GL_NEAREST);

  ctx_pop();

  return false;
}

void light_map_clear(LightMap *self) {
  ctx_push();
  ctx_bind_draw_fbo(self->fbo);

  glClearColor(0.0, 0.0, 0.0, 1.0);
  glClear(GL_COLOR_BUFFER_BIT);

  ctx_pop();
}

void light_map_add(LightMap *self, LightMap *other) {
  ctx_push();
  ctx_bind_draw_fbo(self->fbo);
  ctx_viewport(0, 0, self->width, self->height);

  glEnable(GL_BLEND);
  glBlendFunc(GL_ONE, GL_ONE);

  glBindTexture(GL_TEXTURE_2D, other->tex);

  ctx_use_shader(add_shader);
  glBindVertexArray(tex_overlay_vao);
  glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

  glDisable(GL_BLEND);

  ctx_pop();
}

void light_map_apply_with(LightMap *self, GLuint modulated_tex) {
  ctx_push();

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, modulated_tex);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, self->tex);

  ctx_use_shader(apply_shader);
  ctx_set_uniform_1i("light_map", 0);
  ctx_set_uniform_1i("modulated_tex", 1);
  ctx_set_uniform_1f("gamma", self->gamma);

  glBindVertexArray(tex_overlay_vao);
  glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

  ctx_pop();
}

void light_map_cast_light(LightMap *self, Light light) {
  ctx_push();
  ctx_bind_draw_fbo(self->fbo);
  ctx_viewport(0, 0, self->width, self->height);

  ctx_use_shader(light_shader);

  ctx_set_uniform_2f("size", self->width, self->height);
  ctx_set_uniform_2f("view_pos", self->view_x, self->view_y);
  ctx_set_uniform_2f("view_size", self->view_w, self->view_h);

  ctx_set_uniform_2f("light_pos", light.x, light.y);
  ctx_set_uniform_3f("light_color", light.r, light.g, light.b);
  ctx_set_uniform_1f("light_dist_mul", light.dist_mul);

  glBindVertexArray(overlay_vao);
  glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

  ctx_pop();
}

static void map_coords_to_window(LightMap *self, float *x, float *y) {
  *x = (*x - self->view_x) * self->width / self->view_w;
  *y = (*y - self->view_y) * self->height / self->view_h;
}

static void extend_out_to_inf(float from_x, float from_y, float *x, float *y) {
  const float pretty_much_inf = 100000.0; // This is a hack.
  float dist = hypot(*x - from_x, *y - from_y);
  *x = (*x - from_x) / dist * pretty_much_inf + from_x;
  *y = (*y - from_y) / dist * pretty_much_inf + from_y;
}

static void line_normal(float x1, float y1, float x2, float y2, float *out_x, float *out_y) {
  float nx = -(y2 - y1), ny = x2 - x1;
  float mag = hypot(nx, ny);
  nx /= mag, ny /= mag;
  *out_x = nx, *out_y = ny;
}

void light_map_cast_shadows(LightMap *self, Light light, LightObstacle obstacles[], size_t obstaclec) {
  ctx_push();
  ctx_bind_draw_fbo(self->fbo);
  ctx_viewport(0, 0, self->width, self->height);

  glBindBuffer(GL_ARRAY_BUFFER, self->shadow_vbo);
  glBindVertexArray(self->shadow_vao);
  glBindTexture(GL_TEXTURE_2D, self->tex_copy);

  ctx_use_shader(shadow_shader);
  ctx_set_uniform_2f("size", self->width, self->height);
  float light_x = light.x, light_y = light.y;
  map_coords_to_window(self, &light_x, &light_y);
  ctx_set_uniform_2f("light_pos", light_x, light_y);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  for(size_t i = 0; i < obstaclec; i++) {
    if(obstacles[i].r == 1.0 && obstacles[i].g == 1.0 && obstacles[i].b == 1.0) continue;

    const size_t max_segc = 100; // This is tied to res/shadow.frag

    bool is_shadow_edge[max_segc + 1];
    for(size_t j = 0; j < obstacles[i].vertexc; j++) {
      size_t a = (j + obstacles[i].vertexc - 1) % obstacles[i].vertexc;
      size_t b = (j + 1) % obstacles[i].vertexc;

      float x = obstacles[i].xs[j], y = obstacles[i].ys[j];
      float ax = obstacles[i].xs[a] - x, ay = obstacles[i].ys[a] - y;
      float bx = obstacles[i].xs[b] - x, by = obstacles[i].ys[b] - y;
      x -= light.x, y -= light.y;
      is_shadow_edge[j] = sign(cross(x, y, ax, ay)) == sign(cross(x, y, bx, by));
    }
    if(!obstacles[i].is_closed) {
      is_shadow_edge[0] = true;
      is_shadow_edge[obstacles[i].vertexc - 1] = true;
    }

    float segs[4 * max_segc];
    int should_antialias_edge[2 * max_segc];
    float vertices[6 * 4 * max_segc];
    int seg_ids[4 * max_segc];
    size_t segc = 0, tric = 0;

    size_t possible_segc = obstacles[i].vertexc - (obstacles[i].is_closed && obstacles[i].vertexc > 2 ? 0 : 1);
    assert(possible_segc <= max_segc);
    for(size_t a = 0; a < possible_segc; a++) {
      size_t b = (a + 1) % obstacles[i].vertexc;

      float x1 = obstacles[i].xs[a], y1 = obstacles[i].ys[a];
      float x2 = obstacles[i].xs[b], y2 = obstacles[i].ys[b];
      if(x1 == x2 && y1 == y2) continue;
      map_coords_to_window(self, &x1, &y1);
      map_coords_to_window(self, &x2, &y2);

      int light_side = which_side(light_x, light_y, x1, y1, x2, y2);
      if(light_side == 0) continue;

      segs[4 * segc] = x1;
      segs[4 * segc + 1] = y1;
      segs[4 * segc + 2] = x2;
      segs[4 * segc + 3] = y2;
      should_antialias_edge[2 * segc] = is_shadow_edge[a];
      should_antialias_edge[2 * segc + 1] = is_shadow_edge[b];

      if(which_side(x2, y2, light_x, light_y, x1, y1) < 0) {
        float temp = x1;
        x1 = x2;
        x2 = temp;
        temp = y1;
        y1 = y2;
        y2 = temp;
      }

      float projected_x1 = x1, projected_y1 = y1;
      float projected_x2 = x2, projected_y2 = y2;
      extend_out_to_inf(light_x, light_y, &projected_x1, &projected_y1);
      extend_out_to_inf(light_x, light_y, &projected_x2, &projected_y2);

      float anx, any, bnx, bny, cnx, cny;
      line_normal(projected_x1, projected_y1, x1, y1, &anx, &any);
      line_normal(x1, y1, x2, y2, &bnx, &bny);
      line_normal(x2, y2, projected_x2, projected_y2, &cnx, &cny);

      projected_x1 += anx, projected_y1 += any;
      projected_x2 += cnx, projected_y2 += cny;

      vertices[6 * tric    ] = x1 + bnx;
      vertices[6 * tric + 1] = y1 + bny;
      vertices[6 * tric + 2] = x2 + bnx;
      vertices[6 * tric + 3] = y2 + bny;
      vertices[6 * tric + 4] = projected_x2;
      vertices[6 * tric + 5] = projected_y2;
      seg_ids[tric] = segc;
      tric++;

      vertices[6 * tric    ] = x1 + bnx;
      vertices[6 * tric + 1] = y1 + bny;
      vertices[6 * tric + 2] = projected_x1;
      vertices[6 * tric + 3] = projected_y1;
      vertices[6 * tric + 4] = projected_x2;
      vertices[6 * tric + 5] = projected_y2;
      seg_ids[tric] = segc;
      tric++;

      vertices[6 * tric    ] = x1 + bnx;
      vertices[6 * tric + 1] = y1 + bny;
      vertices[6 * tric + 2] = x1 + anx;
      vertices[6 * tric + 3] = y1 + any;
      vertices[6 * tric + 4] = projected_x1;
      vertices[6 * tric + 5] = projected_y1;
      seg_ids[tric] = segc;
      tric++;

      vertices[6 * tric    ] = x2 + bnx;
      vertices[6 * tric + 1] = y2 + bny;
      vertices[6 * tric + 2] = x2 + cnx;
      vertices[6 * tric + 3] = y2 + cny;
      vertices[6 * tric + 4] = projected_x2;
      vertices[6 * tric + 5] = projected_y2;
      seg_ids[tric] = segc;
      tric++;

      segc++;
    }

    if(obstacles[i].r != 0.0 || obstacles[i].g != 0.0 || obstacles[i].b != 0.0) {
      glCopyImageSubData(self->tex, GL_TEXTURE_2D, 0, 0, 0, 0, self->tex_copy, GL_TEXTURE_2D, 0, 0, 0, 0, self->width, self->height, 1);
    }
    ctx_set_uniform_3f("color", obstacles[i].r, obstacles[i].g, obstacles[i].b);
    ctx_set_uniform_2fv("segs", 2 * segc, segs);
    ctx_set_uniform_1iv("should_antialias_edge", 2 * segc, should_antialias_edge);
    ctx_set_uniform_1iv("seg_ids", tric, seg_ids);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6 * tric, vertices, GL_DYNAMIC_DRAW);
    glDrawArrays(GL_TRIANGLES, 0, 3 * tric);
  }

  glDisable(GL_BLEND);

  ctx_pop();
}

void light_map_add_light(LightMap *self, LightMap *aux, Light light, LightObstacle obstacles[], size_t obstaclec) {
  light_map_cast_light(aux, light);
  light_map_cast_shadows(aux, light, obstacles, obstaclec);
  light_map_add(self, aux);
}
