/*
 * digitful - A 2D lighting simulation in OpenGL
 * Copyright (C) 2020-2022 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <digitful/resource.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <digitful/utils.h>

void resource_init(Resource *self, const char *name) {
  self->name = malloc(sizeof(char) * (strlen(name) + 1));
  strcpy(self->name, name);
  self->size = 0;
  self->data = NULL;
  self->refc = 0;
  self->type = RESOURCE_NONE;
}

void resource_init_from_file(Resource *self, const char *name, const char *filename, bool should_append_null) {
  resource_init(self, name);
  resource_set_to_file(self, filename, should_append_null);
}

void resource_init_from_mem(Resource *self, const char *name, unsigned char *data, size_t size) {
  resource_init(self, name);
  resource_set_to_mem(self, data, size);
}

void resource_deinit(Resource *self) {
  resource_unload(self);
  free(self->name);
}

void resource_set_to_none(Resource *self) {
  resource_unload(self);
  self->type = RESOURCE_NONE;
}

void resource_set_to_file(Resource *self, const char *filename, bool should_append_null) {
  resource_unload(self);
  self->type = RESOURCE_FILE;
  self->file.filename = filename;
  self->file.should_append_null = should_append_null;
}

void resource_set_to_mem(Resource *self, unsigned char *data, size_t size) {
  resource_unload(self);
  self->type = RESOURCE_MEM;
  self->mem.data = data;
  self->mem.size = size;
}

bool resource_load(Resource *self) {
  switch(self->type) {
  case RESOURCE_NONE:
    break;
  case RESOURCE_FILE:
    errno = 0;
    self->data = read_file(self->file.filename, self->file.should_append_null, &self->size);
    if(errno != 0) {
      printf("Failed to load resource from file '%s': %s\n", self->file.filename, strerror(errno));
      return true;
    }
    break;
  case RESOURCE_MEM:
    self->data = self->mem.data;
    self->size = self->mem.size;
    break;
  }
  return false;
}

void resource_unload(Resource *self) {
  if(self->refc > 0) {
    printf("warning: Unloading resource '%s' with non-zero refs\n", self->name);
  }
  switch(self->type) {
  case RESOURCE_NONE:
    break;
  case RESOURCE_FILE:
    free(self->data);
    break;
  case RESOURCE_MEM:
    break;
  }
  self->size = 0;
  self->data = NULL;
}

void* resource_get(Resource *self) {
  if(resource_load(self)) {
    return NULL;
  }
  self->refc++;
  return self->data;
}

void resource_release(Resource *self) {
  if(self->refc == 0) {
    printf("warning: Cannot release a ref to resource '%s' with zero refs\n", self->name);
  } else {
    self->refc--;
  }
}
