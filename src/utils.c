/*
 * digitful - A 2D lighting simulation in OpenGL
 * Copyright (C) 2020-2022 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <digitful/utils.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <GLFW/glfw3.h>

// The biggest primes just below powers of two with minor exceptions in the first few numbers
static const size_t hashmap_sizes[] = {
                                1,
                             2, 5, 7,
                            17, 31, 61,
                          127, 251, 509,
                         1021, 2039, 4093,
                               8191,
                       16381, 32749, 65521,
                      131071, 262139, 524287,
                    1048573, 2097143, 4194301,
                             8388593,
                   16777213, 33554393, 67108859,
                 134217689, 268435399, 536870909,
                1073741789, 2147483647, 4294967291,
                            8589934583,
              17179869143, 34359738337, 68719476731,
             137438953447, 274877906899, 549755813881,
           1099511627689, 2199023255531, 4398046511093,
                          8796093022151,
          17592186044399, 35184372088777, 70368744177643,
        140737488355213, 281474976710597, 562949953421231,
       1125899906842597, 2251799813685119, 4503599627370449,
                         9007199254740847,
     18014398509481951, 36028797018963901, 72057594037927931,
    144115188075855859, 288230376151711717, 576460752303423433,
  1152921504606846883, 2305843009213693951, 4611686018427387847,
                       9223372036854775783,
                       18446744073709551557ull
};

void hashmap_init(HashMap *self, size_t elem_size, HashFunc hash, EqualsFunc equals) {
  self->elem_size = elem_size;
  self->hash = hash;
  self->equals = equals;
  self->elemc = 0;

  self->buckets = NULL;
  self->bucketc = 0;
  self->occupiedc = 0;
  self->max_load_factor = 0.7; // This is just a default value and not some magic constant that holds everything together.
}

HashMap* hashmap_new(size_t elem_size, HashFunc hash, EqualsFunc equals) {
  HashMap *result = malloc(sizeof(HashMap));
  hashmap_init(result, elem_size, hash, equals);
  return result;
}

void hashmap_deinit(HashMap *self) {
  for(size_t i = 0; i < self->bucketc; i++) {
    if(self->buckets[i] != NULL && self->buckets[i] != HASHMAP_UNUSED) {
      free(self->buckets[i]);
    }
  }
  free(self->buckets);
}

void hashmap_destroy(HashMap *self) {
  hashmap_deinit(self);
  free(self);
}

void* hashmap_get(HashMap *self, void *elem) {
  assert(elem != NULL);
  if(self->elemc == 0) {
    return NULL;
  }
  size_t idx = hashmap_lookup(self, elem, false);
  return idx == (size_t) -1 ? NULL : self->buckets[idx];
}

void hashmap_set(HashMap *self, void *elem) {
  assert(elem != NULL);
  if(self->occupiedc + 1 > self->bucketc || (self->occupiedc + 1.0) / self->bucketc > self->max_load_factor) {
    size_t i = 0;
    while(hashmap_sizes[i] <= self->bucketc) {
      i++;
      assert(i < array_size(hashmap_sizes));
    }
    hashmap_rehash(self, hashmap_sizes[i]);
  }

  void *copy = malloc(self->elem_size);
  memcpy(copy, elem, self->elem_size);

  size_t idx = hashmap_lookup(self, elem, true);
  assert(idx != (size_t) -1);
  if(self->buckets[idx] == NULL) {
    self->elemc++;
    self->occupiedc++;
  } else if(self->buckets[idx] == HASHMAP_UNUSED) {
    self->elemc++;
  } else {
    free(self->buckets[idx]);
  }
  self->buckets[idx] = copy;
}

bool hashmap_remove(HashMap *self, void *elem) {
  assert(elem != NULL);
  if(self->elemc == 0) {
    return false;
  }
  size_t idx = hashmap_lookup(self, elem, false);
  if(idx == (size_t) -1 || self->buckets[idx] == NULL) {
    return false;
  }

  free(self->buckets[idx]);
  self->buckets[idx] = HASHMAP_UNUSED;
  self->elemc--;
  return true;
}

void hashmap_rehash(HashMap *self, size_t bucketc) {
  assert(bucketc >= self->elemc);

  void **old_buckets = self->buckets;
  size_t old_bucketc = self->bucketc;
  self->buckets = calloc(bucketc, sizeof(void*));
  self->bucketc = bucketc;
  self->occupiedc = self->elemc;

  for(size_t i = 0; i < old_bucketc; i++) {
    if(old_buckets[i] != NULL && old_buckets[i] != HASHMAP_UNUSED) {
      self->buckets[hashmap_lookup(self, old_buckets[i], true)] = old_buckets[i];
    }
  }
  free(old_buckets);
}

size_t hashmap_lookup(HashMap *self, void *elem, bool should_treat_unused_as_null) {
  assert(elem != NULL);
  if(self->bucketc == 0) {
    return -1;
  }
  size_t hash = self->hash(elem) % self->bucketc;

  size_t i = hash;
  while(true) {
    if(self->buckets[i] == NULL) break;
    if(should_treat_unused_as_null && self->buckets[i] == HASHMAP_UNUSED) break;
    if(self->buckets[i] != HASHMAP_UNUSED && self->equals(self->buckets[i], elem)) break;
    i = (i + 1) % self->bucketc;
    if(i == hash) {
      return -1;
    }
  }
  return i;
}



unsigned char* read_file(const char *filename, bool should_append_null, size_t *out_size) {
  FILE *file = fopen(filename, "rb");
  if(file == NULL) {
    return NULL;
  }

  fseek(file, 0, SEEK_END);
  size_t size = ftell(file);
  if(out_size != NULL) {
    *out_size = size;
  }
  fseek(file, 0, SEEK_SET);

  unsigned char *result = malloc(should_append_null ? size + 1 : size);
  if(should_append_null) {
    result[size] = 0;
  }
  if(fread(result, 1, size, file) != size) {
    free(result);
    result = NULL;
  }

  fclose(file);
  return result;
}

float app_time() {
  return glfwGetTime();
}

// Daniel J. Bernstein's djb2
size_t hash_str(const char *str) {
  size_t hash = 5381;
  for(; *str != '\0'; str++) {
    hash = hash * 33 ^ (size_t) *str;
  }
  return hash;
}

float cross(float ax, float ay, float bx, float by) {
  return ax * by - ay * bx;
}

int sign(float x) {
  return x == 0.0 ? 0 : (x > 0.0 ? 1 : -1);
}

int which_side(float x, float y, float ax, float ay, float bx, float by) {
  return sign(cross(bx - ax, by - ay, x - ax, y - ay));
}

bool do_intersect(float ax1, float ay1, float ax2, float ay2, float bx1, float by1, float bx2, float by2) {
  // This doesn't work when the two segments are colinear.
  int a = which_side(bx1, by1, ax1, ay1, ax2, ay2);
  int b = which_side(bx2, by2, ax1, ay1, ax2, ay2);
  int c = which_side(ax1, ay1, bx1, by1, bx2, by2);
  int d = which_side(ax2, ay2, bx1, by1, bx2, by2);
  return a != b && c != d;
}
