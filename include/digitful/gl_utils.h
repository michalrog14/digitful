/*
 * digitful - A 2D lighting simulation in OpenGL
 * Copyright (C) 2020-2022 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef DIGITFUL_GL_UTILS_H
#define DIGITFUL_GL_UTILS_H

#include <stdbool.h>
#include <stdio.h>
#include <glad/glad.h>
#include <digitful/shader.h>

extern Shader *simple_tex_shader;
extern GLuint overlay_vbo, tex_overlay_vbo;
extern GLuint overlay_vao, tex_overlay_vao;

void gl_utils_init();
void gl_utils_deinit();

GLuint tex2d_new(int width, int height, GLenum format, GLenum min_filter, GLenum mag_filter);
GLuint tex2d_new_from_data(int width, int height, GLenum format, void *data, GLenum data_format, GLenum data_type, GLenum min_filter, GLenum mag_filter);
GLuint tex2d_new_from_file(const char *filename, GLenum min_filter, GLenum mag_filter);
GLuint tex2d_new_from_mem(void *img_data, size_t img_size, GLenum min_filter, GLenum mag_filter);
GLuint tex2d_new_from_gres(const char *name, GLenum min_filter, GLenum mag_filter);
GLuint tex2dms_new(int width, int height, GLenum format, int samplec);

typedef struct {
  GLenum type;
  int count;
} VertexAttribute;

void set_vao_attributes(VertexAttribute attrs[], size_t attrc);

#endif
