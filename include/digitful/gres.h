/*
 * digitful - A 2D lighting simulation in OpenGL
 * Copyright (C) 2020-2022 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef DIGITFUL_GRES_H
#define DIGITFUL_GRES_H

#include <digitful/resource.h>

// gres - global resource

void gres_init();
void gres_deinit();
Resource* gres_get(const char *name);
void gres_release(Resource *res);
void gres_register(Resource res);
void gres_register_file(const char *name, const char *filename, bool should_append_null);
void gres_register_mem(const char *name, unsigned char *data, size_t size);

#endif
