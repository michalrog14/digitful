/*
 * digitful - A 2D lighting simulation in OpenGL
 * Copyright (C) 2020-2022 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef DIGITFUL_RESOURCE_H
#define DIGITFUL_RESOURCE_H

#include <stdbool.h>
#include <stddef.h>

typedef enum {
  RESOURCE_NONE,
  RESOURCE_FILE,
  RESOURCE_MEM
} ResourceType;

typedef struct {
  char *name;
  size_t size;
  unsigned char *data;
  int refc;

  ResourceType type;
  union {
    struct {
      const char *filename;
      bool should_append_null;
    } file;
    struct {
      unsigned char *data;
      size_t size;
    } mem;
  };
} Resource;

void resource_init(Resource *self, const char *name);
void resource_init_from_file(Resource *self, const char *name, const char *filename, bool should_append_null);
void resource_init_from_mem(Resource *self, const char *name, unsigned char *data, size_t size);
void resource_deinit(Resource *self);
void resource_set_to_none(Resource *self);
void resource_set_to_file(Resource *self, const char *filename, bool should_append_null);
void resource_set_to_mem(Resource *self, unsigned char *data, size_t size);
bool resource_load(Resource *self);
void resource_unload(Resource *self);
void* resource_get(Resource *self);
void resource_release(Resource *self);

#endif
