#!/usr/bin/env python3
#
# digitful - A 2D lighting simulation in OpenGL
# Copyright (C) 2020-2022 Karol "digitcrusher" Łacina
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os, sys, shutil, distutils.dir_util, hashlib, typing as ty, time
from subprocess import run

cc = 'gcc'
flags = '-Wall -Wextra -std=c11 -O3 -Iinclude/ -Iexternal/include/'.split()
external_flags = '-O3 -Iexternal/include/ -Iexternal/cimgui/imgui/ -DIMGUI_IMPL_API=extern"C"'.split()
test_flags = flags + '-lglfw -lm -lstdc++ -Iexternal/cimgui/ -Iexternal/cimgui/generator/output/'.split()
should_compile_tests_reses = False

should_compile_for_win = False
if should_compile_for_win:
  target = 'x86_64-w64-mingw32'
  cc = f'{target}-gcc'
  flags += f'-Iexternal/{target}/include/'.split()
  external_flags += f'-Iexternal/{target}/include/'.split()
  test_flags += f'-mwindows -static -Iexternal/{target}/include/ -Lexternal/{target}/lib/'.split()

def join_paths(a, b):
  return os.path.join(a, b)

def path_dir(path):
  return os.path.dirname(path)

def is_file(path):
  return os.path.isfile(path)

def is_dir(path):
  return os.path.isdir(path)

def mkdir(path):
  os.makedirs(path, exist_ok=True)

def rm(path, recursive=False):
  if os.path.isfile(path):
    os.remove(path)
  elif recursive:
    try:
      shutil.rmtree(path)
    except FileNotFoundError:
      pass
  else:
    raise Exception(f"Won't delete directory '{path}' without the recursive flag")

def cp(src, dst):
  os.makedirs(os.path.dirname(dst), exist_ok=True)
  if os.path.isfile(src):
    shutil.copy2(src, dst)
  else:
    shutil.copytree(src, os.path.join(dst, os.path.basename(src)))

def cp_contents(src, dst):
  # shutil.copytree doesn't like merging directories
  distutils.dir_util.copy_tree(src, dst)

def ls(path):
  return os.listdir(path)

def walk_files(path):
  for enclosing, _, files in os.walk(path):
    for file in files:
      yield os.path.join(enclosing, file)

def hash_files(paths, hash=None):
  if hash == None:
    hash = hashlib.sha256()
  for path in paths:
    with open(path, 'rb') as file:
      while True:
        block = file.read(128 * 1024)
        if not block:
          break
        hash.update(block)
  return hash

def hash_strs(strs, hash=None):
  if hash == None:
    hash = hashlib.sha256()
  for str in strs:
    hash.update(str.encode('utf-8'))
  return hash

class Artifact:
  path: os.PathLike
  hash: str
  recreate_func: ty.Callable[[], None]

  cached_path: os.PathLike
  was_cached: bool

  def __init__(self, path, hash, recreate_func):
    self.path = path
    self.hash = hash.hexdigest()
    self.recreate_func = recreate_func

    self.cached_path = join_paths('build/cache/', self.hash)
    if is_file(self.cached_path):
      cp(self.cached_path, self.path)
      self.was_cached = True
    else:
      self.recreate()

  def recreate(self):
    self.recreate_func()
    cp(self.path, self.cached_path)
    self.was_cached = False

def compile_file(src, flags):
  dst = join_paths('build/obj/', src + '.o')
  hash = hash_strs([cc] + flags, hash_files([src]))
  def compile():
    mkdir(path_dir(dst))
    if run([cc, '-c', src] + flags + ['-o', dst]).returncode != 0:
      raise Exception(f"Failed to compile file '{src}'")

  return Artifact(dst, hash, compile)

def compile_exec(srcs, dst, flags):
  if should_compile_for_win:
    dst += '.exe'
  hash = hash_strs([cc] + flags, hash_files(srcs))
  def compile():
    mkdir(path_dir(dst))
    if run([cc] + srcs + flags + ['-o', dst]).returncode != 0:
      raise Exception(f"Failed to compile exec '{dst}'")

  return Artifact(dst, hash, compile)

def link_library(objs, name):
  dst = join_paths('build/', 'lib' + name + '.a')
  hash = hash_files(objs)
  def link():
    mkdir(path_dir(dst))
    if run(['ar', 'rcs', dst] + objs).returncode != 0:
      raise Exception(f"Failed to link library '{name}'")

  return Artifact(dst, hash, link)

glad_obj = None
cimgui_lib = None
def build_external():
  print('-- Compiling glad...')
  global glad_obj
  glad_obj = compile_file('external/src/glad.c', flags=external_flags).path

  print('-- Building cimgui...')
  srcs = [
    'cimgui.cpp',
    'imgui/imgui.cpp',
    'imgui/imgui_demo.cpp',
    'imgui/imgui_draw.cpp',
    'imgui/imgui_tables.cpp',
    'imgui/imgui_widgets.cpp',
    'imgui/backends/imgui_impl_opengl3.cpp',
    'imgui/backends/imgui_impl_glfw.cpp'
  ]
  objs = []
  for src in srcs:
    src = join_paths('external/cimgui/', src)
    print(f"-- Compiling source file '{src}'...")
    objs.append(compile_file(src, flags=external_flags).path)

  print('-- Linking cimgui...')
  global cimgui_lib
  cimgui_lib = link_library(objs, 'cimgui').path

main_lib = None
def build_main_lib():
  print(f"-- Building the main library...")

  objs = [glad_obj]
  for src in walk_files('src/'):
    if not src.endswith('.c'):
      continue

    print(f"-- Compiling source file '{src}'...")
    objs.append(compile_file(src, flags=flags).path)

  print(f"-- Linking the main library...")
  global main_lib
  main_lib = link_library(objs, 'digitful').path

def parse_res(path, name):
  result = f'unsigned char {name}[] = {{\n'
  with open(path, 'rb') as res:
    i = 0
    def add(byte):
      nonlocal result, i
      if i % 13 == 0:
        result += '  '
      result += f'0x{byte:0>2x}, '
      if i % 13 == 12:
        result += '\n'
      i += 1

    for byte in res.read():
      add(byte)
    add(0) # C string null terminator

  if not result.endswith('\n'):
    result += '\n'
  result += '};\n'

  return result

def gen_register_gres():
  reses = list(walk_files('build/test/res/'))

  dst = 'build/obj/register_gres.c'
  hash = hash_strs([str(should_compile_tests_reses)] + reses)
  if should_compile_tests_reses:
    hash = hash_files(reses, hash)
  def parse():
    mkdir(path_dir(dst))
    with open(dst, 'x') as file:
      file.write('#include <stdbool.h>\n')
      file.write('#include <unistd.h>\n')
      file.write('#include <digitful/gres.h>\n')
      file.write('#include <digitful/utils.h>\n\n')

      func = 'void register_gres() {\n'
      for res in reses:
        name = os.path.relpath(res, 'build/test/res/')
        relpath = os.path.relpath(res, "build/test/")
        if should_compile_tests_reses:
          var_name = ''
          for c in res:
            var_name += c if c.isalpha() or c.isdigit() else '_'
          file.write('static ' + parse_res(res, var_name))
          func += f'  if(access("{relpath}", R_OK) == 0) {{\n'
          func += f'    gres_register_file("{name}", "{relpath}", true);\n'
          func +=  '  } else {\n'
          func += f'    gres_register_mem("{name}", {var_name}, array_size({var_name}) - 1);\n'
          func +=  '  }\n'
        else:
          func += f'  gres_register_file("{name}", "{relpath}", true);\n'
      func += '}\n'

      file.write(func)

  return Artifact(dst, hash, parse)

def build_tests():
  print('-- Taking care of tests resources...')
  mkdir('build/test/')
  cp_contents('test/res/', 'build/test/res/')
  cp_contents('res/', 'build/test/res/digitful/')
  res_obj = compile_file(gen_register_gres().path, flags=flags).path

  for test in ls('test/'):
    name = test.rstrip('.c')
    test = join_paths('test/', test)
    exec = join_paths('build/test/', name)

    if not (is_file(test) and test.endswith('.c')):
      continue

    print(f"-- Compiling test '{name}'...")
    compile_exec([test, res_obj, main_lib, cimgui_lib], exec, test_flags)

  if should_compile_tests_reses:
    rm('build/test/res/', True)

def init():
  mkdir('build/')
  for name in ls('build/'):
    path = join_paths('build/', name)
    if not (name == 'cache' and is_dir(path)):
      rm(path, True)

  mkdir('build/cache/')
  for path in ls('build/cache/'):
    path = join_paths('build/cache/', path)
    if is_dir(path):
      rm(path, True)

  mkdir('build/obj/')

def clean():
  rm('build/', True)

def build():
  build_external()
  build_main_lib()
  build_tests()

init()
if __name__ == '__main__':
  start_time = time.perf_counter()

  tasks = sys.argv[1:]
  if not tasks:
    build()
  else:
    for task in tasks:
      if task == 'clean':
        clean()
      elif task == 'build':
        build()
      else:
        raise Exception(f"Unknown task '{task}'")

  print(f'-- Done in {time.perf_counter() - start_time:.2f}s')
