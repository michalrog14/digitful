#version 330 core

out vec4 frag_color;

uniform sampler2D tex;
uniform vec2 size;
uniform vec2 light_pos;

uniform vec3 color;
const int max_segc = 100; // This is tied to src/lights.c
uniform vec2 segs[2 * max_segc];
uniform bool should_antialias_edge[2 * max_segc];
uniform int seg_ids[4 * max_segc];

float cross(vec2 a, vec2 b) {
  return a.x * b.y - a.y * b.x;
}

float which_side(vec2 pt, vec2 a, vec2 b) {
  return sign(cross(b - a, pt - a));
}

float dist_to_line(vec2 pt, vec2 a, vec2 b) {
  return abs(cross(b - a, pt - a)) / distance(a, b);
}

float on_line(vec2 pt, vec2 a, vec2 b) {
  return dot(b - a, pt - a) / pow(distance(a, b), 2.0);
}

void main() {
  int seg_id = seg_ids[gl_PrimitiveID];
  vec2 frag = gl_FragCoord.xy, a = segs[2 * seg_id], b = segs[2 * seg_id + 1];

  bool is_beyond = which_side(frag, a, b) != which_side(light_pos, a, b);
  bool is_between = which_side(frag, light_pos, a) != which_side(frag, light_pos, b);

  float dist_to_edge = 1.0 / 0.0;
  float on_ab = on_line(frag, a, b);
  if(0.0 <= on_ab && on_ab <= 1.0) {
    dist_to_edge = min(dist_to_edge, dist_to_line(frag, a, b));
  }
  if(is_beyond) {
    if(should_antialias_edge[2 * seg_id] && on_line(frag, light_pos, a) >= 1.0) {
      dist_to_edge = min(dist_to_edge, dist_to_line(frag, light_pos, a));
    }
    if(should_antialias_edge[2 * seg_id + 1] && on_line(frag, light_pos, b) >= 1.0) {
      dist_to_edge = min(dist_to_edge, dist_to_line(frag, light_pos, b));
    }
  }

  if(!is_beyond || !is_between) {
    dist_to_edge = -dist_to_edge;
  }
  frag_color = vec4(color * texture(tex, gl_FragCoord.xy / size).rgb, clamp(dist_to_edge + 0.5, 0.0, 1.0));
}
