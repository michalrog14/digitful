#version 330 core

layout(location = 0) in vec2 vert;

uniform vec2 size;

void main() {
  gl_Position = vec4(2.0 * vert / size - 1.0, 0.0, 1.0);
}
