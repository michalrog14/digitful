#version 330 core

out vec4 frag_color;

uniform vec2 size;
uniform vec2 view_pos, view_size;

uniform vec2 light_pos;
uniform vec3 light_color;
uniform float light_dist_mul;

void main() {
  float dist = distance(gl_FragCoord.xy / size * view_size + view_pos, light_pos) / 30.0;
  float intensity = pow(dist * light_dist_mul + 1.0, -2.0);
  frag_color = vec4(light_color * intensity, 1.0);
}
